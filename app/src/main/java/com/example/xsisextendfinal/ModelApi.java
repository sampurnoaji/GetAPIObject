package com.example.xsisextendfinal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ModelApi {

    @SerializedName("weatherObservation")
    @Expose
    private ModelWeather weatherObservation = null;

    @SerializedName("status")
    @Expose
    private ModelStatus status = null;

    public ModelStatus getStatus() {
        return status;
    }

    public void setStatus(ModelStatus status) {
        this.status = status;
    }

    public ModelWeather getWeatherObservation() {
        return weatherObservation;
    }

    public void setWeatherObservation(ModelWeather weatherObservation) {
        this.weatherObservation = weatherObservation;
    }
}
