package com.example.xsisextendfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    private Service service;
    private TextView tvMessage;
    private TextView tvElevation, tvLongitude, tvObservation, tvICAO, tvClouds,
            tvDewPoint, tvCloudsCode, tvDateTime, tvCountryCode, tvTemperature,
            tvHumidity, tvStation, tvWeatherCond, tvWindDirection, tvHecto,
            tvWindSpeed, tvLatitude;
    private EditText etLatitude, etLongitude, etUsername;
    private Button btnGet, btnClear;

    private static Retrofit retrofit;
    private static String BASE_URL = "http://api.geonames.org/";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        setupView();
        networkRequest();
    }

    private void setupView() {
        etLatitude  = findViewById(R.id.et_latitude);
        etLongitude = findViewById(R.id.et_longitude);
        etUsername  = findViewById(R.id.et_username);

        tvMessage   = findViewById(R.id.tv_message);

        tvElevation     = findViewById(R.id.tv_elevation);
        tvLongitude     = findViewById(R.id.tv_longitude);
        tvObservation   = findViewById(R.id.tv_observation);
        tvICAO          = findViewById(R.id.tv_icao);
        tvClouds        = findViewById(R.id.tv_clouds);
        tvDewPoint      = findViewById(R.id.tv_dew_point);
        tvCloudsCode    = findViewById(R.id.tv_clouds_code);
        tvDateTime      = findViewById(R.id.tv_date_time);
        tvCountryCode   = findViewById(R.id.tv_country_code);
        tvTemperature   = findViewById(R.id.tv_temperature);
        tvHumidity      = findViewById(R.id.tv_humidity);
        tvStation       = findViewById(R.id.tv_station_name);
        tvWeatherCond   = findViewById(R.id.tv_weather_condition);
        tvWindDirection = findViewById(R.id.tv_wind_direction);
        tvHecto         = findViewById(R.id.tv_hecto);
        tvWindSpeed     = findViewById(R.id.tv_wind_speed);
        tvLatitude      = findViewById(R.id.tv_latitude);

        btnGet      = findViewById(R.id.btn_get);
        btnClear    = findViewById(R.id.btn_clear);
    }
    
    private void networkRequest(){
        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String inputLat = etLatitude.getText().toString();
                final String inputLng = etLongitude.getText().toString();
                final String inputUsername = etUsername.getText().toString();

                service = getRetrofitInstance().create(Service.class);
                service.getData("http://api.geonames.org/findNearByWeatherJSON?lat=" + inputLat +
                        "&lng=" + inputLng +
                        "&username=" + inputUsername).enqueue(new Callback<ModelApi>() {
                    @Override
                    public void onResponse(Call<ModelApi> call, Response<ModelApi> response) {
                        ModelApi modelApis = response.body();
                        ModelStatus modelStatus;
                        ModelWeather modelWeather;
                        if (modelApis != null) {
                            if (modelApis.getWeatherObservation() != null) {
                                modelWeather = modelApis.getWeatherObservation();
                                tvElevation.setText(modelWeather.getElevation().toString());
                                tvLongitude.setText(modelWeather.getLng().toString());
                                /*tvObservation.setText(modelWeather.getObservation());*/
                                tvICAO.setText(modelWeather.getiCAO());
                                tvClouds.setText(modelWeather.getClouds());
                                tvDewPoint.setText(modelWeather.getDewPoint());
                                tvCloudsCode.setText(modelWeather.getCloudsCode());
                                tvDateTime.setText(modelWeather.getDatetime());
                                tvCountryCode.setText(modelWeather.getCountryCode());
                                tvTemperature.setText(modelWeather.getTemperature());
                                tvHumidity.setText(modelWeather.getHumidity().toString());
                                tvStation.setText(modelWeather.getStationName());
                                tvWeatherCond.setText(modelWeather.getWeatherCondition());
                                tvWindDirection.setText(String.valueOf(modelWeather.getWindDirection()));
                                tvHecto.setText(String.valueOf(modelWeather.getHectoPascAltimeter()));
                                tvWindSpeed.setText(modelWeather.getWindSpeed());
                                tvLatitude.setText(modelWeather.getLat().toString());
                            } else {
                                modelStatus = modelApis.getStatus();
                                String message = modelStatus.getMessage();
                                tvMessage.setText("ERROR\n" + message);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ModelApi> call, Throwable t) {
                        Toast.makeText(MainActivity.this, "Internet Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvElevation.setText(null);
                tvLongitude.setText(null);
                tvObservation.setText(null);
                tvICAO.setText(null);
                tvClouds.setText(null);
                tvDewPoint.setText(null);
                tvCloudsCode.setText(null);
                tvDateTime.setText(null);
                tvCountryCode.setText(null);
                tvTemperature.setText(null);
                tvHumidity.setText(null);
                tvStation.setText(null);
                tvWeatherCond.setText(null);
                tvWindDirection.setText(null);
                tvHecto.setText(null);
                tvWindSpeed.setText(null);
                tvLatitude.setText(null);

                tvMessage.setText(null);
            }
        });
    }

    public static Retrofit getRetrofitInstance(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        if (retrofit == null){
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}
